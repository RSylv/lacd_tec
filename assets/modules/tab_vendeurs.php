<?php

// -----------------------------------------------------------------------------------------------
// TRAITEMENT REQUETE FORM TABLEAU VENDEUR :
// -----------------------------------------------------------------------------------------------

if (isset($_POST["valider_v"])) {

    // On récupere l'id vendeur via la valeur de l'input type="hidden" : 
    $id_v = $_POST['id_vendeur'];

    // Update de l'etat du vendeur : 
    $ajout_vend = $conn->prepare("UPDATE cd_vendeurs SET etat = 1 WHERE id = :id");
    $ajout_vend->bindParam(":id", $id_v);
    $ajout_vend->execute();
}

if (isset($_POST["bloquer_v"])) {

    // On récupere l'id vendeur via la valeur de l'input type="hidden" : 
    $id_v = $_POST['id_vendeur'];

    // Update de l'etat du vendeur : 
    $ajout_vend = $conn->prepare("UPDATE cd_vendeurs SET etat = 0 WHERE id = :id");
    $ajout_vend->bindParam(":id", $id_v);
    $ajout_vend->execute();
}

?>



<!-- ----------------------------------------------------------------------------------------- -->
<!-- LISTE DES VENDEURS : -->
<!-- ----------------------------------------------------------------------------------------- -->

<div class="admin_vendeur admin">

    <table>

        <thead>
            <tr>
                <th colspan="7" class="titre_tab">
                    <h2>Nouveaux vendeurs inscrits en attente de validation :</h2>
                </th>
            </tr>
            <tr>
                <th>ID</th>
                <th>NOM</th>
                <th>PRENOM</th>
                <th>EMAIL</th>
                <th>ETAT</th>
                <th>VALIDER LE VENDEUR</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <?php
            // Recuperer le tableau des vendeurs : 
            $req_vend = $conn->prepare("SELECT id, nom, prenom, email, if( etat = 1, 'VALID', 'NOT VALID')as etat, if( connecte = 1, 'CONNECTE', 'HORS LIGNE')as connecte FROM cd_vendeurs ORDER BY id DESC");
            $req_vend->execute();
            $vendeurs = $req_vend->fetchAll();

            // l'afficher dans un tableau :
            foreach ($vendeurs as $v) { ?>
                <tr>
                    <td><?php printf("%s", $v['id']); ?></td>
                    <td><?php printf("%s", $v['nom']); ?></td>
                    <td><?php printf("%s", $v['prenom']); ?></td>
                    <td><?php printf("%s", $v['email']); ?></td>
                    <?php if ($v['etat'] == 'NOT VALID') { ?>
                        <td class="alert_off"><?php printf("%s", $v['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_vendeur" value="<?php printf("%s", $v['id']); ?>">
                                <input class="form_btn btn_off" type="submit" name="valider_v" value="VALIDER">
                            </form>
                        </td>
                    <?php } ?>
                    <?php if ($v['etat'] == 'VALID') { ?>
                        <td class="alert_on"><?php printf("%s", $v['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_vendeur" value="<?php printf("%s", $v['id']); ?>">
                                <input class="form_btn btn_on" style="background:#a81414ab;color:#fff;" type="submit" name="bloquer_v" value="Bloquer">
                            </form>
                        </td>
                        <?php if ($v['connecte'] == 'HORS LIGNE') { ?>
                            <td class="alert_off"><?php printf("%s", $v['connecte']); ?></td>
                        <?php } ?>
                        <?php if ($v['connecte'] == 'CONNECTE') { ?>
                            <td class="alert_on"><?php printf("%s", $v['connecte']); ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>

        <tfoot>
            <tr>
            </tr>
        </tfoot>

    </table>

</div>