<?php
// -----------------------------------------------------------------------------------------------
// TRAITEMENT REQUETE FORM TABLEAU VENDEUR :
// -----------------------------------------------------------------------------------------------

if (isset($_POST["valider_u"])) {

    // On récupere l'id vendeur via la valeur de l'input type="hidden" : 
    $id_u = $_POST['id_user'];

    // Update de l'etat du vendeur : 
    $user = $conn->prepare("UPDATE cd_users SET etat = 1 WHERE id = :id");
    $user->bindParam(":id", $id_u);
    $user->execute();
}

if (isset($_POST["bloquer_u"])) {

    // On récupere l'id vendeur via la valeur de l'input type="hidden" : 
    $id_u = $_POST['id_user'];

    // Update de l'etat du vendeur : 
    $user = $conn->prepare("UPDATE cd_users SET etat = 0 WHERE id = :id");
    $user->bindParam(":id", $id_u);
    $user->execute();
}
?>



<!-- ----------------------------------------------------------------------------------------- -->
<!-- LISTE DES VENDEURS : -->
<!-- ----------------------------------------------------------------------------------------- -->

<div class="admin_utilisateur admin">

    <table>

        <thead>
            <tr>
                <th colspan="7" class="titre_tab">
                    <h2>Utilisateurs du site :</h2>
                </th>
            </tr>
            <tr>
                <th>ID</th>
                <th>NOM</th>
                <th>PRENOM</th>
                <th>EMAIL</th>
                <th>ETAT</th>
                <th>BLOQUER USER</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            <?php
            // Recuperer le tableau des utilisateurs : 
            $req_user = $conn->prepare("SELECT id, nom, prenom, email, if( etat = 1, 'VALID', 'NOT VALID')as etat, if( connecte = 1, 'CONNECTE', 'HORS LIGNE')as connecte FROM cd_users ORDER BY id DESC");
            $req_user->execute();
            $users = $req_user->fetchAll();

            // l'afficher dans un tableau :
            foreach ($users as $u) { ?>
                <tr>
                    <td><?php printf("%s", $u['id']); ?></td>
                    <td><?php printf("%s", $u['nom']); ?></td>
                    <td><?php printf("%s", $u['prenom']); ?></td>
                    <td><?php printf("%s", $u['email']); ?></td>
                    <?php if ($u['etat'] == 'NOT VALID') { ?>
                        <td class="alert_off"><?php printf("%s", $u['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_user" value="<?php printf("%s", $u['id']); ?>">
                                <input class="form_btn btn_on" type="submit" name="valider_u" value="VALIDER">
                            </form>
                        </td>
                    <?php } ?>
                    <?php if ($u['etat'] == 'VALID') { ?>
                        <td class="alert_on"><?php printf("%s", $u['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_user" value="<?php printf("%s", $u['id']); ?>">
                                <input class="form_btn btn_off" type="submit" name="bloquer_u" value="Bloquer">
                            </form>
                        </td>
                        <?php if ($u['connecte'] == 'HORS LIGNE') { ?>
                            <td class="alert_off"><?php printf("%s", $u['connecte']); ?></td>
                        <?php } ?>
                        <?php if ($u['connecte'] == 'CONNECTE') { ?>
                            <td class="alert_on"><?php printf("%s", $u['connecte']); ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>

        <tfoot>
            <tr>
            </tr>
        </tfoot>

    </table>

</div>