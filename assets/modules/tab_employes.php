<?php
$error = [];
$auth = null;


//  -------------------------------------------------------------------------------------------
//  TRAITEMENT DU FORMULAIRE D'INSCRIPTION DE L'EMPLOYE :
//  -------------------------------------------------------------------------------------------
if (isset($_POST['employe'])) {

    // Verifier si les données sont bien présentent :
    $data = [$_POST['user_name'], $_POST['user_firstname'], $_POST['user_email'], $_POST['password']];
    foreach ($data as $value) {
        if (isset($value) && !empty($value)) {
            addslashes($value);
            continue;
        } else {
            $error['champs_vide'] = 'Veuillez remplir TOUS les champs ci-dessous';
            break;
        }
    }

    // Verification du format mail selon PHP :
    if (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Email incorrect';
    } else {

        // Si les données sont bien présentent ALORS securisation :   
        $type = 'EMPLOYE';
        $class = 'PERSONNEL';
        $name = secure($_POST['user_name']);
        $firstname = secure($_POST['user_firstname']);
        $email = secure($_POST['user_email']);
        $password = secure($_POST['password']);

        // Verifier si l'email existe deja dans la base de données :
        $mail_v = $conn->prepare("SELECT * FROM cd_employes WHERE email = :email");
        $mail_v->bindParam(":email", $email, PDO::PARAM_STR);
        $mail_v->execute();
        if ($mail_v->fetch()) {
            $error['doublon'] = 'Cette adresse mail est déjà utilisée.';
        } else {
            // Requete BDD :
            $req = $conn->prepare("INSERT INTO cd_employes VALUES (null, '$type', '$class', '$name', '$firstname', '$email', '$password', 0, 0)");
            try {
                $req->execute();
            } catch (PDOException $th) {
                print $th->getMessage();
                $error['pdo'] = "Problème de connexion.. veuillez ressayer";
            }

            // Si l'enregistrement a fonctionné :
            $auth = "L'employé a bien été enregistré.";
        }
    }
}
?>


<!-- -------------------------------------------------------------------------------------- -->
<!-- FORMULAIRE POUR INSCRIRE UN EMPLOYE : -->
<!-- -------------------------------------------------------------------------------------- -->
<div class="inscription">

    <h2>Ajouter un employé :</h2>

    <!-- Alert si il y a une erreur : -->
    <?php if ($error) : ?>
        <div class="alert_off">
            <?php foreach ($error as $err) : ?>
                <li><?php printf('%s', $err); ?></li>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <!-- Alert si tout a fonctionné : -->
    <?php if ($auth) : ?>
        <div class="alert_on">
            <li><?php printf('%s', $auth); ?></li>
        </div>
    <?php endif; ?>

    <!-- formulaire d'inscription : -->
    <form action="" method="post">
        <input value="<?php if (isset($_POST['user_name'])) {
                            printf("%s", $_POST['user_name']);
                        } ?>" class="input" type="text" name="user_name" placeholder="Nom">
        <input value="<?php if (isset($_POST['user_name'])) {
                            printf("%s", $_POST['user_firstname']);
                        } ?>" class="input" type="text" name="user_firstname" placeholder="Prenom">
        <input value="<?php if (isset($_POST['user_name'])) {
                            printf("%s", $_POST['user_email']);
                        } ?>" class="input" type="email" name="user_email" placeholder="Email">
        <input class="input" type="password" name="password" placeholder="Password" autocomplete="new-password"> <br>
        <!-- <input type="password" name="verifpassword" placeholder="Verification password" > <br> -->
        <hr>

        <input class="input form_btn" type="submit" name="employe" value="Enregistrer l'employé">

    </form>

</div>


<!-- -------------------------------------------------------------------------------------- -->
<!-- BLOQUER OU VALIDER L'ACCES AU SITE D'UN EMPLOYES : -->
<!-- -------------------------------------------------------------------------------------- -->
<?php
if (isset($_POST["valider_e"])) {

    // On récupere l'id employé via la valeur de l'input type="hidden" : 
    $id_v = $_POST['id_employe'];

    // Update de l'etat de l'employé : 
    $ajout_vend = $conn->prepare("UPDATE cd_employes SET etat = 1 WHERE id = :id");
    $ajout_vend->bindParam(":id", $id_v);
    $ajout_vend->execute();
}

if (isset($_POST["bloquer_e"])) {

    // On récupere l'id employe via la valeur de l'input type="hidden" : 
    $id_v = $_POST['id_employe'];

    // Update de l'etat de l'employé : 
    $ajout_vend = $conn->prepare("UPDATE cd_employes SET etat = 0 WHERE id = :id");
    $ajout_vend->bindParam(":id", $id_v);
    $ajout_vend->execute();
}
?>


<!-- -------------------------------------------------------------------------------------- -->
<!-- AFFICHER LA LISTE DES EMPLOYES -->
<!-- -------------------------------------------------------------------------------------- -->
<div class="admin_employe admin">

    <table>

        <thead>
            <tr>
                <th colspan="6" class="titre_tab">
                    <h2>Liste des employés :</h2>
                </th>
            </tr>
            <tr>
                <th>NOM</th>
                <th>PRENOM</th>
                <th>EMAIL</th>
                <th>ETAT</th>
                <th>BLOQUER ACCES</th>
            </tr>
        </thead>

        <tbody>
            <?php
            // Recuperer le tableau des employés : 
            $req_vend = $conn->prepare("SELECT id, nom, prenom, email, if( etat = 1, 'VALID', 'NOT VALID')as etat, if( connecte = 1, 'CONNECTE', 'HORS LIGNE')as connecte FROM cd_employes ORDER BY id DESC");
            $req_vend->execute();
            $employe = $req_vend->fetchAll();

            // l'afficher dans un tableau :
            foreach ($employe as $e) { ?>
                <tr>
                    <td><?php printf("%s", $e['nom']); ?></td>
                    <td><?php printf("%s", $e['prenom']); ?></td>
                    <td><?php printf("%s", $e['email']); ?></td>
                    <?php if ($e['etat'] == 'NOT VALID') { ?>
                        <td class="alert_off"><?php printf("%s", $e['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_employe" value="<?php printf("%s", $e['id']); ?>">
                                <input class="form_btn" style="background:#189c18ab;color:#fff;" type="submit" name="valider_e" value="VALIDER">
                            </form>
                        </td>
                    <?php } ?>
                    <?php if ($e['etat'] == 'VALID') { ?>
                        <td class="alert_on"><?php printf("%s", $e['etat']); ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_employe" value="<?php printf("%s", $e['id']); ?>">
                                <input class="form_btn" style="background:#a81414ab;color:#fff;" type="submit" name="bloquer_e" value="Bloquer">
                            </form>
                        </td>
                    <?php } ?>
                    <?php if ($e['connecte'] == 'HORS LIGNE') { ?>
                        <td class="alert_off"><?php printf("%s", $e['connecte']); ?></td>
                    <?php } ?>
                    <?php if ($e['connecte'] == 'CONNECTE') { ?>
                        <td class="alert_on"><?php printf("%s", $e['connecte']); ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>

        <tfoot>
            <tr>
            </tr>
        </tfoot>

    </table>

</div>



<!-- ------------------------------------------------------------------------------------- -->
<!-- END OF BODY -->
<!-- ------------------------------------------------------------------------------------- -->
<?php
include 'include/footer.php';
?>