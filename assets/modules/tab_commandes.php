<!-- ------------------------------------------------------------------------------------------- -->
<!-- TRAITEMENT DES CHOIX D'AFFICHAGE : -->
<!-- ------------------------------------------------------------------------------------------- -->

<?php
if (isset($_POST['etat_req'])) {
    $id_c = $_POST['id_cmd'];
    $etat = $_POST['etat'];

    $req_etat = $conn->prepare("UPDATE cd_commandes SET etat = :etat WHERE id = :id_c");
    $req_etat->bindParam(":id_c", $id_c);
    $req_etat->bindParam(":etat", $etat);
    $req_etat->execute();
}

// VALIDER COMMANDE :
if (isset($_POST['valid_cmd'])) {
    $id_c = $_POST['id_cmd'];

    $req_cmd_v = $conn->prepare("UPDATE cd_commandes SET validation = 1 WHERE id = :id_c");
    $req_cmd_v->bindParam(":id_c", $id_c);
    $req_cmd_v->execute();
}
// BLOQUER COMMANDE :
if (isset($_POST['bloq_cmd'])) {
    $id_c = $_POST['id_cmd'];

    $req_cmd_b = $conn->prepare("UPDATE cd_commandes SET validation = 0 WHERE id = :id_c");
    $req_cmd_b->bindParam(":id_c", $id_c);
    $req_cmd_b->execute();
}

// QUANTITE ++ :
if (isset($_POST['p_plus'])) {
    $id_c = $_POST['id_c'];
    $qt_p = $_POST['qt_c'];
    $qt_p++;

    $req_plus = $conn->prepare("UPDATE cd_commandes SET quantite = :qt WHERE id = :id");
    $req_plus->bindParam(":qt", $qt_p, PDO::PARAM_INT);
    $req_plus->bindParam(":id", $id_c);
    $req_plus->execute();
}
// QUANTITE -- :
if (isset($_POST['p_moins'])) {
    $id_c = $_POST['id_c'];
    $qt_p = $_POST['qt_c'];
    $qt_p--;

    $req_moins = $conn->prepare("UPDATE cd_commandes SET quantite = :qt WHERE id = :id");
    $req_moins->bindParam(":qt", $qt_p, PDO::PARAM_INT);
    $req_moins->bindParam(":id", $id_c);
    $req_moins->execute();;
}
?>


<!-- AFFICHER INFO VENDEUR : -->
<?php
if (isset($_POST['vendeur'])) {

    // On récupere la valeur de des input:hidden : 
    $id_c = $_POST['id_cmd'];
    $id_v = $_POST['id_vendeur'];

    // Update de l'etat du vendeur : 
    $req_etat = $conn->prepare("UPDATE cd_commandes SET etat = :etat WHERE id = :id_c");
    $req_etat->bindParam(":id_c", $id_c);
    $req_etat->bindParam(":etat", $etat);
    $req_etat->execute();
}
?>



<!-- ------------------------------------------------------------------------------------------- -->
<!-- AFFICHER LA LISTE DES COMMANDES : -->
<!-- ------------------------------------------------------------------------------------------- -->
<?php
$commd = $conn->prepare("SELECT * FROM cd_commandes");
$commd->execute();
$commande = $commd->fetchAll();
if ($commande) : ?>
    <div class="admin_produit admin">

        <table>

            <thead>
                <tr>
                    <th colspan="13" class="titre_tab">
                        <h2>Commandes passées cette dernière semaine :</h2>
                    </th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>NUMERO</th>
                    <th>DATE</th>
                    <th>ID PRODUIT</th>
                    <th>QUANTITE</th>
                    <!-- <th>DENOMINATION PRODUIT</th> -->
                    <th>PRIX U.</th>
                    <th>PRIX TOTAL</th>
                    <th>ID VENDEUR</th>
                    <!-- <th>NOM VENDEUR</th> -->
                    <th>ID ACHETEUR</th>
                    <!-- <th>NOM ACHETEUR</th> -->
                    <th>ADRESSE</th>
                    <th>VALIDATION</th>
                    <th>ETAT</th>
                    <th>INFOS</th>
                </tr>
            </thead>

            <tbody>
                <?php
                $total = 0;
                $total_cmd = 0;
                foreach ($commande as $c) :
                    // Recuperer le nom du vendeur :
                    $vendeur = $conn->prepare("SELECT * FROM cd_vendeurs WHERE id = :id_v");
                    $vendeur->bindParam(":id_v", $c['id_vendeur']);
                    $vendeur->execute();
                    $v = $vendeur->fetch();

                    // Recuperer le nom de l'acheteur :
                    $acheteur = $conn->prepare("SELECT * FROM cd_users WHERE id = :id_a");
                    $acheteur->bindParam(":id_a", $c['id_acheteur']);
                    $acheteur->execute();
                    $a = $acheteur->fetch();

                    // Recuperer le produit concerné :
                    $produit = $conn->prepare("SELECT * FROM cd_produits WHERE id = :id_p");
                    $produit->bindParam(":id_p", $c['id_produit']);
                    $produit->execute();
                    $p = $produit->fetch();

                ?>
                    <tr>
                        <td><?php printf("%s", $c['id']); ?></td>
                        <td>#<?php printf("%s", $c['numero']); ?></td>
                        <td><?php printf("%s", $c['cmd_date']); ?></td>
                        <td><?php printf("%s", $c['id_produit']); ?>
                            <form action="" method="post">
                                <input type="hidden" name="id_produit" value="<?php printf("%s", $c['id_produit']); ?>">
                                <input type="submit" name="produit" value="+">
                            </form>
                        </td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="id_c" value="<?php printf("%s", $c['id']); ?>">
                                <input type="hidden" name="qt_c" value="<?php printf("%s", $c['quantite']); ?>">
                                <input type="submit" value="-" name="p_moins">
                            </form>
                            <?php printf("%s", $c['quantite']); ?>
                            <form action="" method="post">
                                <input type="hidden" name="id_c" value="<?php printf("%s", $c['id']); ?>">
                                <input type="hidden" name="qt_c" value="<?php printf("%s", $c['quantite']); ?>">
                                <input type="submit" value="+" name="p_plus">
                            </form>

                        </td>
                        <td><?php printf("%s", $c['prix']); ?></td>
                        <td><?php
                            for ($i = 0; $i < $c['quantite']; $i++) {
                                $total += $c['prix'];
                            }
                            printf("%s", $total); ?> €</td>
                        <td><?php printf("%s", $c['id_vendeur']); ?>
                            <form action="" method="post">
                                <input type="hidden" name="id_cmd" value="<?php printf("%s", $c['id']); ?>">
                                <input type="hidden" name="id_vendeur" value="<?php printf("%s", $c['id_vendeur']); ?>">
                                <input type="submit" name="vendeur" value="+">
                            </form>
                        </td>
                        <td><?php printf("%s", $c['id_acheteur']); ?>
                            <form action="" method="post">
                                <input type="hidden" name="id_acheteur" value="<?php printf("%s", $c['id_acheteur']); ?>">
                                <input type="submit" name="acheteur" value="+">
                            </form>
                        </td>
                        <td><?php printf("%s", $c['adresse']); ?></td>
                        <td><?php
                            if ($c['validation'] == 0) { ?>
                                <p class="alert_off">BLOQUE</p>
                                <form action="" method="post">
                                    <input type="hidden" name="id_cmd" value="<?php printf("%s", $c['id']); ?>">
                                    <input class="form_btn btn_on" type="submit" value="Valider ?" name="valid_cmd">
                                </form>
                            <?php } else { ?>
                                <form action="" method="post">
                                    <p class="alert_on">VALIDER</p>
                                    <input type="hidden" name="id_cmd" value="<?php printf("%s", $c['id']); ?>">
                                    <input class="form_btn btn_off" type="submit" value="Bloquer ?" name="bloq_cmd">
                                </form>
                            <?php } ?>
                        </td>
                        <td><?php printf("%s", $c['etat']); ?>
                            <form action="" method="post">
                                <select name="etat">
                                    <option value="TRAITEMENT">TRAITEMENT</option>
                                    <option value="ENVOYE">ENVOYE</option>
                                    <option value="BIEN RECU">BIEN RECU</option>
                                    <option value="EN ATTENTE">EN ATTENTE</option>
                                    <option value="ANNULE">ANNULE</option>
                                </select>
                                <input type="hidden" name="id_cmd" value="<?php printf("%s", $c['id']); ?>">
                                <input type="submit" name="etat_req" value="go">
                            </form>
                        </td>
                        <td><?php printf("%s", $c['infos_supp']); ?></td>
                    </tr>
                <?php $total_cmd += $total;
                endforeach; ?>
            </tbody>

        </table>
        <br>
        <p class="underline"><em>Le total des commandes du tableau : </em><span class="type_compte"><?php printf("%s", $total_cmd);  ?></span> €</p>
    </div>
<?php else : ?>
    <br>
    <br>
    <br>
    <p>Aucune commande cette semaine sur le site...</p>
<?php endif; ?>