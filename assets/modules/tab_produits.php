<!-- ------------------------------------------------------------------------------------------- -->
<!-- REQUETE BDD POUR ACTIVER / DESACTIVER LE PRODUIT : -->
<!-- ------------------------------------------------------------------------------------------- -->
<?php
if (isset($_POST['activer_p'])) {
    $id_p = $_POST['id_p'];
    
    $req_actif = $conn->prepare("UPDATE cd_produits SET actif = 1 WHERE id = :id_p");
    $req_actif->bindParam(":id_p", $id_p);
    $req_actif->execute();

}

if (isset($_POST['bloquer_p'])) {
    $id_p = $_POST['id_p'];

    $req_actif = $conn->prepare("UPDATE cd_produits SET actif = 0 WHERE id = :id_p");
    $req_actif->bindParam(":id_p", $id_p);
    $req_actif->execute();
}
?>

<!-- ------------------------------------------------------------------------------------------- -->
<!-- REQUETE BDD POUR RECUPERER LES PRODUITS : -->
<!-- ------------------------------------------------------------------------------------------- -->
<?php
$req_prod = $conn->prepare("SELECT * FROM cd_produits");
$req_prod->execute();
$produits = $req_prod->fetchAll(); 
?>


<!-- ------------------------------------------------------------------------------------------- -->
<!-- AFFICHER LA LISTE DES PRODUITS DANS UN TABLEAU : -->
<!-- ------------------------------------------------------------------------------------------- -->
<?php if ($produits) : ?>
    <div class="admin_produit admin">

        <table>

            <thead>
                <tr>
                    <th colspan="10" class="titre_tab">
                        <h2>Produits en vente sur le site : </h2>
                    </th>
                </tr>
                <tr>
                    <th class="marge" colspan="8">PRODUITS</th>
                    <th class="marge" colspan="2">VENDEURS</th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>TYPE</th>
                    <th>TITRE</th>
                    <th>IMAGE</th>
                    <th>AUTEUR/REALISATION</th>
                    <th>STOCK</th>
                    <th>PRIX</th>
                    <th>ACTIF</th>
                    <th>NOM</th>
                    <th>EMAIL</th>
                </tr>
            </thead>

            <tbody>
                <?php
                foreach ($produits as $p) :
                    // Recuperer le nom du vendeur :
                    $vendeur = $conn->prepare("SELECT * FROM cd_vendeurs WHERE id = :id_v");
                    $vendeur->bindParam(":id_v", $p['id_vendeur']);
                    $vendeur->execute();
                    $v = $vendeur->fetch();
                ?>
                    <tr>
                        <td><?php printf("%s", $p['id']); ?></td>
                        <td><?php printf("%s", $p['type']); ?></td>
                        <td><?php printf("%s", $p['titre']); ?></td>
                        <td><?php if ($p['image']) { ?>
                                <img style="height:1.5rem;width:auto;" src="<?php printf("%s", $p['image']); ?>" alt="image jaquette">
                            <?php } else { ?>
                                Pas d'image ..
                            <?php } ?>
                        </td>
                        <td><?php printf("%s", $p['interprete']); ?></td>
                        <td><?php printf("%s", $p['stock']); ?></td>
                        <td><?php printf("%s", $p['prix']); ?></td>
                        <?php if ($p['actif'] === '0') { ?>
                            <td class="alert_off">
                            <form action="" method="post">
                                <input type="hidden" name="id_p" value="<?php printf("%s", $p['id']); ?>">
                                <input class="form_btn btn_on" type="submit" name="activer_p" value="Valider?">
                            </form></td>
                        <?php } else { ?>
                            <td class="alert_on">
                            <form action="" method="post">
                                <input type="hidden" name="id_p" value="<?php printf("%s", $p['id']); ?>">
                                <input class="form_btn btn_off" type="submit" name="bloquer_p" value="Bloquer?">
                            </form></td>
                        <?php } ?>
                        <td><?php printf("%s", $v['nom']); ?></td>
                        <td><?php printf("%s", $v['email']); ?></td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                </tr>
            </tfoot>

        </table>

    </div>
<?php endif; ?>