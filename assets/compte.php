<?php
$title = 'CD_TEC Mon compte';
$go = '';
include 'include/header.php';
include 'config/database.php';
?>

<!-- --------------------------------------------------------------------------------------------- -->
<!-- ORIENTAION VERS PAGES SELON TYPE DE COMPTE CONNECTE : -->
<!-- --------------------------------------------------------------------------------------------- -->

<div class="page_compte">
    <!-- Gestion des differents types de compte -->
    <?php
    switch ($_SESSION['type']) {


            // Compte Acheteur :
        case 'ACHETEUR': ?>
            <!-- Mettre ici les possibilités du compte acheteur  -->
            <div class="compte compte_acheteur">
            <h2>Mon Compte :</h2>
            <h3>Bienvenue sur la gestion de votre compte <span class="type_compte">Client :</span></h3>
            <hr>
            <?php include 'acheteur.php'; ?>
            </div>
        <?php break;


            // Compte Vendeur : 
        case 'VENDEUR': ?>
            <!-- Mettre ici les possibilités du compte vendeur  -->
            <div class="compte compte_vendeur">
                <?php if ($_SESSION['etat'] == 1) { ?>
                    <h2>Mon Compte :</h2>
                    <h3>Bienvenue sur la gestion de votre compte <span class="type_compte">Professionnel :</span></h3>
                    <hr>
                    <?php include 'vendeur.php'; ?>
                <?php } else { ?>
                    <br>
                    <br>
                    <p class="alert_on">Votre Compte est en attente de validation, ou a été bloqué par l'administrateur .. <br>
                        Une fois l'autorisation reçue, vous pourrez ajouter des produits sur le site. <br>
                        Merci de votre comprehension.</p>
            </div>
            <?php } ?>
        <?php break;


            // Compte Employé : 
        case 'EMPLOYE': ?>
            <!-- Mettre ici les possibilités du compte employé  -->
            <div class="compte compte_employe">
            <p class="type_compte">Compte Employé</p>
            <?php
            if ($_SESSION["class"] == 'PERSONNEL') { ?>
                <a href="compte.php?go=1">commandes</a>
                <a href="compte.php?go=2">produits</a>
                <hr>
            <?php
                switch ($_GET['go']) {
                    case '1':
                        include 'modules/tab_commandes.php';
                        break;
                    case '2':
                        include 'modules/tab_produits.php';
                        break;
                }
            } ?>
            <hr>
        </div>
        <?php break;


            // Compte Admin : 
        case 'ADMIN': ?>
            <!-- Mettre ici les possibilités du compte Administrateur  -->
            <div class="compte compte_admin">
            <p class="type_compte">Compte Admin</p>
                <?php
                if ($_SESSION["class"] == 'PERSONNEL') { ?>
                    <a class="admlink" href="compte.php?go=1">commandes</a>
                    <a class="admlink" href="compte.php?go=2">produits</a>
                    <a class="admlink" href="compte.php?go=3">vendeurs</a>
                    <a class="admlink" href="compte.php?go=4">employes</a>
                    <a class="admlink" href="compte.php?go=5">utilisateurs</a>
                    <hr>
                <?php
                include 'config/function.php';
                    switch ($_GET['go']) {
                        case '1':
                            include 'modules/tab_commandes.php';
                            break;
                        case '2':
                            include 'modules/tab_produits.php';
                            break;
                        case '3':
                            include 'modules/tab_vendeurs.php';
                            break;
                        case '4':
                            include 'modules/tab_employes.php';
                            break;
                        case '5':
                            include 'modules/tab_utilisateurs.php';
                            break;
                    }
                } ?>
                <?php #include 'admin.php'; 
                ?>
            </div>
        <?php break;


            // Sinon :
        default:
            header('Location: connexion.php');
            exit();
            break;
        ?>

    <?php }
    ?>
</div>



<!-- --------------------------------------------------------------------------------------------- -->
<!-- END OF COMPTE : -->
<!-- --------------------------------------------------------------------------------------------- -->

<?php
include 'include/footer.php';
?>