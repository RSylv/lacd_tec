<?php
session_start();
$prod_titre = '';
// include 'etat_check.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <link rel="stylesheet" href="css/style.min.css">

    <!-- ------------------------------------------------------------------------------------------- -->
    <!-- AFFICHAGE DU TITRE EN FONCTION DE LA PAGE CHARGEE : -->
    <!-- ------------------------------------------------------------------------------------------- -->

    <title>
        <?php if (isset($title)) : ?>
            <?= $title ?>
        <?php else : ?>
            CD_TEC
        <?php endif ?>
    </title>

</head>

<body>

    <!-- ------------------------------------------------------------------------------------------- -->
    <!-- HEADER : -->
    <!-- ------------------------------------------------------------------------------------------- -->

    <header>

        <!-- Lien HOME -->
        <div class="home">
            <a href="<?php if (empty($_SESSION) || $_SESSION["class"] != 'PERSONNEL') : ?>
            index.php <?php else : ?>#<?php endif; ?>"><img class="logo" src="./image/cd_tec_logo.png" alt="">CD_TEC</a>
        </div>

        <?php
        if (empty($_SESSION) || $_SESSION["class"] !== 'PERSONNEL') : ?>
            <!-- incrustation du module Search_bar.php -->
            <div class="search_bar"><?php include 'include/search_bar.php'; ?></div>
        <?php endif; ?>

        <!-- Si une session est ouverte :  -->
        <?php if (isset($_SESSION) && !empty($_SESSION)) : ?>
            <div class="welcome texte">
                Welcome <em><span class="prenom"><?php printf('%s', $_SESSION["firstname"]); ?></span> <span class="nom"><?php printf('%s', $_SESSION["name"]); ?></span></em>

                <?php
                if ($_SESSION["class"] !== 'PERSONNEL') : ?>
                    <!-- Lien vers la page Mon compte :  -->
                    <a href="compte.php">Mon compte</a>
                <?php endif; ?>


                <!-- Si la session est du type Acheteur : -->
                <?php if ($_SESSION["type"] === 'ACHETEUR') : ?>
                    <!-- Alors on crée un lien vers le panier de l'acheteur :  -->
                    <a href="panier.php"><img class="panier" src="./image/shopping-cart.svg" alt=""></a>
                <?php endif; ?>

                <!-- Lien vers la page de deconnexion : -->
                <a href="config/deconnexion.php">Se Deconnecter</a>
            </div>
        <?php else : ?>
            <!-- Si il n'y a pas de session :  -->
            <div class="conn texte"><a href="connexion.php">Se Connecter</a></div>
        <?php endif; ?>

    </header>

    <!-- ------------------------------------------------------------------------------------------- -->
    <!-- END OF HEADER : -->
    <!-- ------------------------------------------------------------------------------------------- -->