<?php
include './config/database.php';


// Si click sur rechercher :
if (isset($_GET['q']) and !empty($_GET['q'])) {
    // $q = htmlspecialchars($_GET['q']);

    // $search = $conn->prepare('SELECT * FROM cd_produits WHERE actif = 1 AND titre LIKE "%' . $q . '%"');
    // $search->execute();
    // $search_bar_results = $search->fetchAll();

    // // Get the keyword from query string
    $keyW = htmlspecialchars($_GET['q']);
    // // Prepare the command
    $search = $conn->prepare('SELECT * FROM cd_produits WHERE actif = 1 AND titre LIKE :keyword');
    // // Put the percentage sing on the keyword
    $keyword = "%" . $keyW . "%";
    // // Bind the parameter
    $search->bindParam(':keyword', $keyword, PDO::PARAM_STR);
    $search->execute();
    $search_bar_results = $search->fetchAll();
    
}
?>

<!-- HTML  -->
<form action="" method="GET">
    <input type="search" name="q" placeholder="Recherche... ">
    <input type="submit" value="Valider">
</form>