<?php
include 'function.php';
include 'database.php';
session_start();
switch ($_SESSION["type"]) {
    case 'ACHETEUR':
        $con_user = $conn->prepare("UPDATE cd_users SET connecte = 0 WHERE id = :id");
        $con_user->bindParam(':id', $_SESSION["id"], PDO::PARAM_INT);
        $con_user->execute();
        break;

    case 'VENDEUR':
        $con_user = $conn->prepare("UPDATE cd_vendeurs SET connecte = 0 WHERE id = :id");
        $con_user->bindParam(':id', $_SESSION["id"], PDO::PARAM_INT);
        $con_user->execute();
        break;

    case 'EMPLOYE':
        $con_user = $conn->prepare("UPDATE cd_employes SET connecte = 0 WHERE id = :id");
        $con_user->bindParam(':id', $_SESSION["id"], PDO::PARAM_INT);
        $con_user->execute();
        break;
}
unset($_SESSION);
session_destroy();
header("Location: ../index.php");
exit();
