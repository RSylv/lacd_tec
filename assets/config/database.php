<?php

try {
    $conn = new PDO('mysql:host=db_host;db_name=db_name;chartset=utf8', 'db_user', 'db_pass');
} catch (PDOException $e) {
    printf('%s', 'Connexion échouée : ' . $e->getMessage());
}
