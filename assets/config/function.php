<?php

function secure(string $d = ''): string
{
    $d = addslashes($d);
    $d = trim($d);
    $d = htmlspecialchars($d);
    // $d = htmlentities($d);
    // $d = strip_tags($d);
    // $d = stripslashes($d);
    return $d;
}
