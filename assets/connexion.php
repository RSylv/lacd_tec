<?php
$title = 'CD_TEC Connexion';
include 'include/header.php';
$error = [];
include 'config/function.php';
include 'config/database.php';


// ---------------------------------------------------------------------------------------------------
// TRAITEMENT DES DONNEES DU FORMULAIRE DE CONNEXION :
// ---------------------------------------------------------------------------------------------------

if (isset($_POST['submit'])) :

    $type = secure($_POST['type']);

    // Si les données sont bien présentent :
    if ((isset($_POST['user_email']) && !empty($_POST['user_email'])) && (isset($_POST['password']) && !empty($_POST['password']))) :

        // Si l'email correspond au format mail selon PHP :
        if (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) :
            $error['email'] = 'Email incorrect';
        else :
            // Securisation des données :
            $user_email = secure($_POST['user_email']);
            $user_pass = secure($_POST['password']);

            if ($type == 'CLI') :

                $req = $conn->prepare('SELECT * FROM cd_users WHERE email = :email');
                $req->bindParam(":email", $user_email, PDO::PARAM_STR);
                $req->execute();
                $req_user = $req->fetch();

                if (!$req_user) :
                    $error['email'] = 'Cet email n\'est pas enregistré sur le site.. retentez ou inscrivez-vous !';
                else :

                    $pass_hash = $req_user['password'];
                    $pass_check = password_verify($user_pass, $pass_hash);

                endif;

            elseif ($type == 'PRO') :

                $req = $conn->prepare('SELECT * FROM cd_vendeurs WHERE email = :email UNION SELECT * FROM cd_employes WHERE email = :email');
                $req->bindParam(":email", $user_email, PDO::PARAM_STR);
                $req->execute();
                $req_user = $req->fetch();

                if (!$req_user) :
                    $error['email'] = 'Cet email n\'est pas enregistré sur le site.. retentez ou inscrivez-vous !';
                else :

                    $pass_hash = $req_user['password'];
                    $pass_check = password_verify($user_pass, $pass_hash);

                endif;

            else :
                $error['compte'] = 'On ne change pas la valeur des inputs ;) !! <br> Autrement dit, votre type de compte est invalide.';
            endif;

            // Si login/password sont OK : ON REMPLI LES VARIABLES DE SESSION :
            if ($pass_check) :
                if ($req_user['etat'] !== '0') :

                    $_SESSION["id"] = $req_user["id"];
                    $_SESSION["type"] = $req_user["type"];
                    $_SESSION["class"] = $req_user["class"];
                    $_SESSION["name"] = $req_user["nom"];
                    $_SESSION["email"] = $req_user["email"];
                    $_SESSION["firstname"] = $req_user["prenom"];
                    $_SESSION["etat"] = $req_user["etat"];

                    // Indiquer que la personne est connectée :
                    switch ($_SESSION["type"]) {
                        case 'ACHETEUR':
                            $con_user = $conn->prepare("UPDATE cd_users SET connecte = 1 WHERE id = :id");
                            $con_user->bindParam(':id', $req_user["id"]);
                            $con_user->execute();
                            break;

                        case 'VENDEUR':
                            $con_user = $conn->prepare("UPDATE cd_vendeurs SET connecte = 1 WHERE id = :id");
                            $con_user->bindParam(':id', $req_user["id"]);
                            $con_user->execute();
                            break;

                        case 'EMPLOYE':
                            $con_user = $conn->prepare("UPDATE cd_employes SET connecte = 1 WHERE id = :id");
                            $con_user->bindParam(':id', $req_user["id"]);
                            $con_user->execute();
                            break;
                    }

                    if ($req_user["class"] === 'PERSONNEL') :
                        header("Location: compte.php");
                        exit();
                    else :
                        header("Location: index.php");
                        exit();
                    endif;

                else :
                    $error['etat'] = "Votre compte n'a pas encore ete validé par l'administrateur <br>
                    ou a été bloqué par celui-ci. <br> Veuillez patienter ou contacter le service technique.";
                endif;

            else :
                $error['inscription'] = 'Le mot de passe saisi est incorrect.';
            endif;

        endif;

    else :
        $error['vide'] = 'Veuillez remplir TOUS les champs ci dessous ! ';
    endif;

endif;
?>


<!-- --------------------------------------------------------------------------------------------- -->
<!-- FORMULAIRE DE CONNEXION : -->
<!-- --------------------------------------------------------------------------------------------- -->

<!-- HTML -->
<div class="connexion">

    <h2>Connexion a votre espace Membre :</h2>

    <!-- Affichage des erreurs, si il y en a : -->
    <?php if ($error) : ?>
        <div class="alert_off">
            <?php foreach ($error as $err) : ?>
                <li><?php printf('%s', $err); ?></li>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <!-- Formulaire Connexion : -->
    <form action="#" method="post">

        <input type="radio" name="type" value="CLI" checked>Compte client
        <input type="radio" name="type" value="PRO">Compte pro
        <br>

        <input class="input" type="email" name="user_email" placeholder="Email"> <br>
        <input class="input" type="password" name="password" placeholder="Password"> <br>
        <hr>
        <input class="input form_btn" type="submit" name="submit" value="Se connecter">

    </form>

    <p>Pas encore inscrit ? <a href="inscription.php"><em>cliquer ici</em></a></p>

</div>



<!-- --------------------------------------------------------------------------------------------- -->
<!-- END OF BODY : -->
<!-- --------------------------------------------------------------------------------------------- -->
<?php
include 'include/footer.php';
?>