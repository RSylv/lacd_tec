<?php
$title = 'CD-TEC Panier';
$error = [];
$valid = [];
include 'include/header.php';
include 'config/function.php';
include 'config/database.php';

// --------------------------------------------------------------------------------------------------
// PANIER :
// --------------------------------------------------------------------------------------------------
$id_acheteur = $_SESSION['id'];



// --------------------------------------------------------------------------------------------------
// AJOUTER UN PRODUIT AU PANIER :
// --------------------------------------------------------------------------------------------------
if (isset($_POST['ajouter'])) {

    $id_produit = $_POST['id_produit'];
    $id_vendeur = $_POST['id_vendeur'];

    // Creation de la table cd_panier_user_(id) :
    $create_ = $conn->prepare("CREATE TABLE IF NOT EXISTS cd_panier_userid_$id_acheteur
    (
        id INT(7) PRIMARY KEY NOT null auto_increment,
        id_vendeur int(7) not null,
        id_acheteur int(7) not null,
        id_produit int(7) not null
    )");
    $create_->execute();

    // Enregistrement du panier dans la table cd_panier_user_(id) :
    $req = $conn->prepare("INSERT INTO cd_panier_userid_$id_acheteur VALUES(null, '$id_vendeur', '$id_acheteur ', '$id_produit')");
    if ($req->execute()) {
        $valid['panier'] = 'Produit bien ajouter au panier !';
    } else {
        $error['panier'] = "Problème avec l'ajout du produit..";
    }
}

// --------------------------------------------------------------------------------------------------
// REQUETE QUI RCUPERERE LES INFOS DU PANIER_USER :
// --------------------------------------------------------------------------------------------------

$pan = $conn->prepare("SELECT * FROM cd_panier_userid_$id_acheteur");
$pan->execute();
$panier_user = $pan->fetchAll();
?>


<!-- -------------------------------------------------------------------------------------------- -->
<!-- CREATION DU BOUTON "VIDER LE PANIER" SI ELEMENTS DANS PANIER : -->
<!-- -------------------------------------------------------------------------------------------- -->

<!-- HTML  -->
<h2>Mon Panier :</h2>
<?php
if ($panier_user) { ?>
    <!-- Vider le panier : -->
    <form action="#" method="post">
        <input class="vider" type="submit" value="Vider le Panier" name="vider">
    </form>
<?php } ?>
<hr>


<!-- -------------------------------------------------------------------------------------------- -->
<!-- AFFICHAGE DES ELEMENTS DU PANIER SI IL Y EN A : -->
<!-- -------------------------------------------------------------------------------------------- -->

<?php
if ($panier_user) {
    $max = sizeof($panier_user);
    for ($i = 0; $i < $max; $i++) {

        // Creer des variables pour le formulaire de commandes :
        $id_v = $panier_user[$i]["id_vendeur"];
        $id_p = $panier_user[$i]["id_produit"];


        // Recuperer infos produit :
        $produit = $conn->prepare("SELECT * FROM cd_produits WHERE id = :id_produits");
        $produit->bindParam(":id_produits", $id_p);
        $produit->execute();
        $produit_user = $produit->fetchAll();
        // var_dump($produit_user);
        // @var :
        $prix_p = $produit_user[0]["prix"];
        $stock_p = $produit_user[0]["stock"];
?>
        <!-- AFFICHAGE :  -->
        <div class="prod_panier">
            <div>
                <p class="p_head">
                    <span class="p_type"><?= sprintf('%s', $produit_user[0]["type"]); ?></span> de
                    <span class="p_interp"><?= sprintf('%s', $produit_user[0]['interprete']); ?></span>
                </p>
                <h3 class="p_titre"><?= sprintf('%s', $produit_user[0]['titre']); ?></h3>
                <p class="p_prix">Prix : <span><?= sprintf('%s', $produit_user[0]['prix']); ?></span> €
            </div>
        </div>

<?php
    }
}


// --------------------------------------------------------------------------------------------------
// SUPPRIMER LE PANIER, SI CLICK SUR BOUTON "VIDER" :
// --------------------------------------------------------------------------------------------------
if (isset($_POST['vider'])) {
    // Supprimer le panier crée :
    $DropBBB = $conn->prepare("DROP TABLE IF EXISTS cd_panier_userid_$id_acheteur");
    if ($DropBBB->execute()) {
        $valid['vider'] = 'Votre panier été vidé !';
        header("Location: panier.php");
    }
}


// --------------------------------------------------------------------------------------------------
// TRAITER LES DONNEES DU FORMULAIRE DE COMMANDE SI CLICK SUR BOUTON "COMMANDER" :
// --------------------------------------------------------------------------------------------------

// Ajouter le panier a la table commandes et supprimer ce dernier :
if (isset($_POST['commander'])) {

    if (isset($_POST["adresse"]) && !empty($_POST["adresse"])) {

        // On crée les variables et leur traitements avant insertion dans BDD ;        
        $timestamp = date("Y-m-d H:i:s");
        $id_vendeur = secure($_POST["id_vendeur"]);
        $id_acheteur = secure($_SESSION['id']);
        $id_produit = secure($_POST["id_produit"]);
        // $quantite = is_numeric($_POST["quantite"]);
        $adresse = secure($_POST["adresse"]);
        $adresse = addslashes($adresse);
        $prix = $prix_p;
        $infos_ = secure($_POST["infos_supp"]);
        $infos_ = addslashes($infos_);
        $numero = ($id_vendeur . $id_acheteur . $id_produit);


        // Enregistrement du panier dans la BDD cd_commandes :
        $rq = $conn->prepare("INSERT INTO cd_commandes VALUES (null, '$numero', '$timestamp', '$id_vendeur', '$id_acheteur ', '$id_produit', '1', '$prix', '$adresse', 0,'TRAITEMENT', '$infos_')");
        if ($rq->execute()) {
            $valid['rec'] = 'Commande enregistrée';
        } else {
            $error['rec'] = "Problème avec l'enregistrement de votre commande.";
        }

        // -1 au stock disponible du produit :
        --$stock_p;
        echo ' <br> new stock : ' . $stock_p;
        $stock = $conn->prepare("UPDATE cd_porduits SET stock = '.$stock_p.' WHERE id = :id_produits");
        $produit->bindParam(":id_produits", $id_p);



        // Supprimer le panier crée :
        $DropBBB = $conn->prepare("DROP TABLE IF EXISTS cd_panier_userid_$id_acheteur");
        if ($DropBBB->execute()) {
            $valid['commande'] = 'Votre commande a bien été prise en compte.';
        }
    } else {
        $error['adresse'] = 'Veuillez nous communiquer votre adressse.';
    }
}
?>



<!-- -------------------------------------------------------------------------------------------- -->
<!-- FORMULAIRE DE COMMANDE SI ELEMENTS DANS LE PANIER : -->
<!-- -------------------------------------------------------------------------------------------- -->

<?php
if ($panier_user) { ?>

    <!-- HTML  -->
    <div class="commande">

        <h2>Commander :</h2>
        <!-- Affichage des erreurs, si il y en a : -->
        <?php if ($error) : ?>
            <div class="alert_off">
                <?php foreach ($error as $err) : ?>
                    <li><?php printf('%s', $err); ?></li>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <!-- Affichage des validations et infos, si il y en a : -->
        <?php if ($valid) : ?>
            <div class="alert_on">
                <?php foreach ($valid as $v) : ?>
                    <li><?php printf('%s', $v); ?></li>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <!-- Button Commander : -->
        <form action="" method="post">
            <input type="hidden" name="id_user" value="<?php printf("%s", $id_a); ?>">
            <input type="hidden" name="id_vendeur" value="<?php printf("%s", $id_p); ?>">
            <input type="hidden" name="id_produit" value="<?php printf("%s", $id_v); ?>">
            <!-- <input type="hidden" name="quantite" value="<?php #printf("%s", $qtp); ?>"> -->

            <input type="text" name="adresse" placeholder="Votre Adresse complète"> <br>
            <textarea name="infos_supp" cols="38" rows="3"></textarea> <br>

            <input class="form_btn commander" type="submit" name="commander" value="Commander">
        </form>
    </div>

<?php
} else { ?>
    <br>
    <br>
    <br>
    <br>
    Votre panier est vide ...
<?php } ?>