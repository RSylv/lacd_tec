<?php
$commd = $conn->prepare("SELECT * FROM cd_commandes WHERE id_acheteur = :id_a");
$commd->bindParam(":id_a", $_SESSION["id"], PDO::PARAM_STR);
$commd->execute();
$commande = $commd->fetchAll();
if ($commande) : 
$total = 0;
?>
    <div class="admin_produit admin">
        <table>
            <thead>
                <tr>
                    <th colspan="10" class="titre_tab">
                        <h2>Recapitulatif de vos commandes :</h2>
                    </th>
                </tr>
                <tr>
                    <th>NUMERO</th>
                    <th>DATE</th>
                    <th>PRODUIT</th>
                    <th>QUANTITE</th>
                    <th>PRIX UNITAIRE</th>
                    <th>PRIX TOTAL</th>
                    <th>MAIL VENDEUR</th>
                    <th>ADRESSE</th>
                    <th>ETAT</th>
                    <th>INFOS</th>
                </tr>
            </thead>

            <tbody>
                <?php
                $total_cmd = 0;
                foreach ($commande as $c) :
                    if ($c['validation'] !== '0') {
                        // Recuperer le nom du vendeur :
                        $vendeur = $conn->prepare("SELECT * FROM cd_vendeurs WHERE id = :id_v");
                        $vendeur->bindParam(":id_v", $c['id_vendeur']);
                        $vendeur->execute();
                        $v = $vendeur->fetch();

                        // Recuperer le nom de l'acheteur :
                        $acheteur = $conn->prepare("SELECT * FROM cd_users WHERE id = :id_a");
                        $acheteur->bindParam(":id_a", $c['id_acheteur']);
                        $acheteur->execute();
                        $a = $acheteur->fetch();

                        // Recuperer le produit concerné :
                        $produit = $conn->prepare("SELECT * FROM cd_produits WHERE id = :id_p");
                        $produit->bindParam(":id_p", $c['id_produit']);
                        $produit->execute();
                        $p = $produit->fetch();

                ?>
                        <tr>
                            <td>#<?php printf("%s", $c['numero']); ?></td>
                            <td><?php printf("%s", $c['cmd_date']); ?></td>
                            <td><?php printf("%s", $p['titre']); ?></td>
                            <td><?php printf("%s", $c['quantite']); ?></td>
                            <td><?php printf("%s", $c['prix']); ?></td>
                            <td><?php
                                for ($i = 0; $i < $c['quantite']; $i++) {
                                    $total += $c['prix'];
                                }
                                printf("%s", $total); ?> €</td>
                            <td><?php printf("%s", $v['email']); ?></td>
                            <td><?php printf("%s", $c['adresse']); ?></td>
                            <td><?php printf("%s", $c['etat']); ?></td>
                            <td><?php printf("%s", $c['infos_supp']); ?></td>
                        </tr>
                <?php $total_cmd += $total;
                    }
                    else { ?>
                        <li class="alert_on">Votre commande a bien ete prise en compte. Elle est en attente d'activation.</li>
                   <?php }
                endforeach; ?>                                
            </tbody>
        </table>
        <br>
        <p class="underline"><em>Le total de votre commande est de : </em><span class="type_compte"><?php printf("%s", $total_cmd);  ?></span> €</p>
    </div>

<?php else : ?>
    <br>
    <br>
    <br>
    <p>Vous n'avez pas encore de commandes sur le site.</p>
<?php endif; ?>