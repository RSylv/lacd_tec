<?php
$search_bar_results = null;
include 'include/header.php';
include 'config/database.php';

// Preparer requete pour afficher les images en page d'accueil :
$req_img = $conn->query('SELECT `image` FROM cd_produits LIMIT 0,5');
$req_img->execute();
$images = $req_img->fetchAll();

?>


<h1 class="texte">La CD-tec Vous propose un large choix de musiques et de films, en vente ou en location !</h1>
<hr>
<div class="resultats">

    <?php
    // Afficher les resultats de la recherche : 
    if (isset($_GET['q']) and !empty($_GET['q'])) {

        // Si il y a des titres correspondants a la recherche :
        if ($search_bar_results) {

            foreach ($search_bar_results as $d) : ?>
                <li><?php printf('%s', $d["titre"]); ?></li>
                <div class="produits">
                    <?php // Si la SESSION est ouverte on affiche le produit complet :
                    if (isset($_SESSION) && !empty($_SESSION)) { ?>
                        <div>

                            <!-- Affichage produit : -->
                            <p class="p_head">
                                <span class="p_type"><?= sprintf('%s', $d['type']); ?></span> de
                                <span class="p_interp"><?= sprintf('%s', $d['interprete']); ?></span>
                            </p>
                            <h2 class="p_titre"><?= sprintf('%s', $d['titre']); ?></h2>
                            <img class="p_img" src="<?= sprintf('%s', $d['image']); ?>" alt="cette image contient la jaquette de <?= sprintf('%s', $d['titre']); ?>">

                            <p class="p_genre">Genre : <span><?= sprintf('%s', $d['genre']); ?></span></p>
                            <p class="p_annee">Année : <span><?= sprintf('%s', $d['annee']); ?></span></p>
                            <p class="p_prix">Prix : <span><?= sprintf('%s', $d['prix']); ?></span> €
                                <img id="click" class="p_click" src="image/plus-square-solid.svg" alt="Cliquer ici pour plus d'infos">
                            </p>

                            <p id="descr" class="p_descr">Description : <br> <span><?= sprintf('%s', $d['description']); ?></p>

                            <?php if ($_SESSION['type'] == 'ACHETEUR') : ?>
                                <!-- Bouton pour Ajouter au panier : -->
                                <form method="post" action="panier.php">
                                    <input type="hidden" name="id_produit" value="<?= sprintf('%s', $d['id']); ?>">
                                    <input type="hidden" name="id_vendeur" value="<?= sprintf('%s', $d['id_vendeur']); ?>">
                                    <input type="submit" value="Ajouter au panier" name="ajouter">
                                </form>
                            <?php endif; ?>

                        </div>
                    <?php
                    } ?>
                </div>
            <?php
            endforeach;
        } else { ?>
            <div class="no_results">
                <p>No results found ...</p> 
            </div>
    <?php }
    }; ?>

</div>


<?php if (!$search_bar_results) { ?>
    <div class="presentation">

        <?php foreach ($images as $i) : ?>
            <div>
                <img src="<?= sprintf('%s', $i['image']); ?>" alt="">
            </div>
        <?php endforeach; ?>

    </div>
<?php } ?>
<hr>

<?php
include 'include/footer.php';
?>