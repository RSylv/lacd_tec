<?php
$error = [];
$auth = null;
include 'config/function.php';
include 'config/database.php';

// ----------------------------------------------------------------------------------------------
// REQUETE POUR AFFICHER LES PRODUITS DU COMPTE :
// ----------------------------------------------------------------------------------------------

$vendeur = $conn->prepare("SELECT * FROM cd_produits WHERE id_vendeur=:id_vendeur");
$vendeur->bindParam(":id_vendeur", $_SESSION["id"]);
$vendeur->execute();
$prod_vendeur = $vendeur->fetchAll();


// ----------------------------------------------------------------------------------------------
// TRAITEMENT DES DONNES DU FORMULAIRE AJOUT DU PRODUIT :
// ----------------------------------------------------------------------------------------------

if (isset($_POST['submit'])) {

    // Recuperation des données saisies dans un tableau :
    $data = [
        $_POST['type'],
        $_POST['titre'],
        $_POST['genre'],
        $_POST['interprete'],
        $_POST['image'],
        $_POST['annee'],
        $_POST['prix'],
        $_POST['description']
    ];

    // Verifier si les données sont bien présentent :
    foreach ($data as $d) {
        if (isset($d) && !empty($d)) {
            addslashes($d);
            continue;
        } else {
            $error['champs_vide'] = 'Veuillez remplir TOUS les champs ci-dessous';
        }
    }

    // Verifier que le type du produit resste ceux attendu :
    // if ($_POST['type'] !== 'CD' || $_POST['type'] !== 'DVD') {
    // $error['type'] = 'Veuillez ne pas changer le type de produit ! ;)';
    // } else {

    // Si les données sont bien présentent ALORS securisation :   
    $type = secure($_POST['type']);
    $titre = secure($_POST['titre']);
    $genre = secure($_POST['genre']);
    $interprete = secure($_POST['interprete']);
    $img = secure($_POST['image']);
    $annee = secure($_POST['annee']);
    $prix = secure($_POST['prix']);
    $desc = secure($_POST['description']);
    $desc = addslashes($desc);
    $id_vendeur = $_SESSION["id"];
    if (!is_numeric($_POST["quantite"])) {
        $error['quantite'] = 'entrez une valeur numerique pour la quantité !';
    } 
    else {
        $stock = $_POST["quantite"];
        // Requete BDD :
        $req = $conn->prepare("INSERT INTO cd_produits VALUES (null, '$type', '$titre', '$genre', '$interprete', '$img', '$annee', '$prix', '$desc', '$id_vendeur', '$stock', 0)");
        // var_dump($req);
        if ($req->execute()) {
            // Si l'enregistrement a fonctionné :
            $auth = 'Le produit a bien etait enregitrer.';
        }
    }
    // }
}
?>


<!-- ---------------------------------------------------------------------------------------- -->
<!-- FORMULAIRE D'ENREGISTREMENT D'UN NOUVEAU PRODUIT : -->
<!-- ---------------------------------------------------------------------------------------- -->

<div class="fomrmulaire_V">
    <!-- HTML -->
    <form class="form_vendeur" action="" method="post">

        <!-- Afficher les erreurs si il y en a :  -->
        <?php if ($error) : ?>
            <div class="alert_off">
                <?php foreach ($error as $err) : ?>
                    <li><?php printf('%s', $err); ?></li>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <!-- Afficher la validation si validation :  -->
        <?php if ($auth) : ?>
            <div class="alert_on">
                <li><?php printf('%s', $auth); ?></li>
            </div>
        <?php endif; ?>

        <h3>Ajouter un produit a la vente :</h3>
        <br>
        <div class="type">
            <input type="radio" value="CD" name="type" checked>CD
            <input type="radio" value="DVD" name="type">DVD
        </div>
        <br>
        <div class="formV">
            <div class="formV_1">
                <div>
                    <input class="input" type="text" name="titre" placeholder="Titre">
                    <input class="input" type="text" name="genre" placeholder="Genre">
                    <input class="input" type="text" name="interprete" placeholder="Interprete">
                </div>
                <div>
                    <input class="input" type="text" name="image" placeholder="Lien image">
                    <input class="input" type="number" min="1900" max="2025" name="annee" placeholder="Année">
                    <input class="input" type="number" name="prix" step=".01" placeholder="Prix">
                </div>
            </div>
            <div class="formV_2">
                <textarea name="description" max="1000" cols="27" rows="8" placeholder="Description .."></textarea>
                <input type="number" name="quantite" placeholder="Quantité"> <br>
                <input class="input form_btn" type="submit" name="submit" value="Enregistrer">
            </div>
        </div>


    </form>
</div>
<hr>


<!-- ---------------------------------------------------------------------------------------- -->
<!-- AFFICHAGE DES PRODUITS DU VENDEURS SI IL Y EN A : -->
<!-- ---------------------------------------------------------------------------------------- -->

<?php if ($prod_vendeur) : ?>

    <div class="mes_produits">

        <?php foreach ($prod_vendeur as $val) : ?>
            <div>
                <p class="p_head">
                    <span class="p_type"><?= sprintf('%s', $val['type']); ?></span> de
                    <span class="p_interp"><?= sprintf('%s', $val['interprete']); ?></span>
                </p>
                <h2 class="p_titre"><?= sprintf('%s', $val['titre']); ?></h2>
                <img class="p_img" src="<?= sprintf('%s', $val['image']); ?>" alt="cette image contient la jaquette de <?= sprintf('%s', $val['titre']); ?>">

                <p class="p_genre">Genre : <span><?= sprintf('%s', $val['genre']); ?></span></p>
                <p class="p_annee">Année : <span><?= sprintf('%s', $val['annee']); ?></span></p>
                <p class="p_prix">Prix : <span><?= sprintf('%s', $val['prix']); ?></span> €</p>
                <hr>
                <p class="p_descr">Description : <br> <span><?= sprintf('%s', $val['description']); ?></p>
            </div>
        <?php endforeach; ?>

    </div>

    <hr>

<?php endif; ?>