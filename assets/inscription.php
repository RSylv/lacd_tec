<?php
// titre de la page :
$title = 'CD_TEC Inscription';
include 'include/header.php';

// Instanciation des erreurs d'affichage :
$error = [];
$auth = null;
include 'config/function.php';
include 'config/database.php';


// ---------------------------------------------------------------------------------------------
// TRAITER LES DONNEES DU FORMULAIRE D'INSCRIPTION
// ---------------------------------------------------------------------------------------------

if (isset($_POST['submit'])) :

    $data = [
        'user_name'      => $_POST['user_name'],
        'type'           => $_POST['type'],
        'user_firstname' => $_POST['user_firstname'],
        'user_email'     => $_POST['user_email'],
        'password'       => $_POST['password'],
        'verifpassword'  => $_POST['verifpassword']
    ];
    (bool) $checked = true;
    foreach ($data as $value) : (isset($value) && !empty($value)) ? addslashes($value) : $checked = false;
    endforeach;

    if (!$checked) :
        $error['champs_vide'] = 'Veuillez remplir TOUS les champs ci-dessous';
    else :
        if (!filter_var($data['user_email'], FILTER_VALIDATE_EMAIL)) :
            $error['email'] = 'Email incorrect';
        else :

            $type = secure($data['type']);
            $class = 'USER';
            $name = secure($data['user_name']);
            $firstname = secure($data['user_firstname']);
            $email = secure($data['user_email']);

            if (strlen($data['password']) < 6) :
                $error['password_length'] = 'Le password ne doit contenir au moins 6 caractères';
            else :

                if ($data['password'] != $data['verifpassword']) :
                    $error['password'] = 'Les passwords ne correspondent pas..';
                else :

                    $password = secure($data['password']);
                    $pass_hash = password_hash($password, PASSWORD_DEFAULT);

                    // Verifier si l'email existe deja dans la base de données :
                    $mail_u = $conn->prepare("SELECT * FROM cd_users WHERE email = :email ");
                    $mail_u->bindParam(":email", $email, PDO::PARAM_STR);
                    $mail_u->execute();
                    $mail_v = $conn->prepare("SELECT * FROM cd_vendeurs WHERE email = :email");
                    $mail_v->bindParam(":email", $email, PDO::PARAM_STR);
                    $mail_v->execute();

                    if ($type == 'ACHETEUR') :
                        if ($mail_u->fetch()) {
                            $error['doublon'] = 'Cette adresse mail est déjà utilisée.';
                        } else {
                            // Requete BDD :
                            $req = $conn->prepare("INSERT INTO cd_users VALUES (null, '$type', '$class', '$name', '$firstname', '$email', '$pass_hash', 0, 0)");
                            try {
                                $req->execute();
                            } catch (PDOException $th) {
                                print $th->getMessage();
                                $error['pdo'] = "Problème de connexion.. veuillez ressayer";
                            }
                            unset($data);
                            unset($_POST);
                            // Si l'enregistrement a fonctionné :
                            $auth = 'Création du compte réussie ! Vous pouvez maintenant vous connecter ;)';
                        }
                    endif;

                    if ($type == 'VENDEUR') :
                        if ($mail_v->fetch()) {
                            $error['doublon'] = 'Cette adresse mail est déjà utilisée.';
                        } else {
                            // Requete BDD :
                            $req = $conn->prepare("INSERT INTO cd_vendeurs VALUES (null, '$type', '$name', '$firstname', '$email', '$pass_hash', 0, 0)");
                            try {
                                $req->execute();
                            } catch (PDOException $th) {
                                print $th->getMessage();
                                $error['pdo'] = "Problème de connexion.. veuillez ressayer";
                            }
                            unset($data);
                            unset($_POST);
                            // Si l'enregistrement a fonctionné :
                            $auth = 'Création du compte réussie ! Vous pouvez maintenant vous connecter ;)';
                        }
                    endif;

                endif;

            endif;

        endif;

    endif;

endif;
?>


<!-- --------------------------------------------------------------------------------------- -->
<!-- FORMULAIRE D'INSCRIPTION -->
<!-- --------------------------------------------------------------------------------------- -->

<!-- HTML -->
<div class="inscription">

    <h2>Créez un compte et rejoignez-nous !</h2>

    <!-- Alert si il y a une erreur : -->
    <?php if ($error) : ?>
        <div class="alert_off">
            <?php foreach ($error as $err) : ?>
                <li><?php printf('%s', $err); ?></li>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <!-- Alert si tout a fonctionné : -->
    <?php if ($auth) : ?>
        <div class="alert_on">
            <li><?php printf('%s', $auth); ?></li>
        </div>
    <?php endif; ?>

    <!-- formulaire d'inscription : -->
    <form action="" method="post">

        <div class="type">
            <p><em>Vous etes :</em></p>
            <input type="radio" value="ACHETEUR" name="type" checked> Acheteur
            <input type="radio" value="VENDEUR" name="type"> Vendeur <br>
        </div>
        <hr>
        <input value="<?php if (isset($_POST['user_name'])) {
                            printf("%s", $_POST['user_name']);
                        } ?>" class="input" type="text" name="user_name" placeholder="Nom"> <br>
        <input value="<?php if (isset($_POST['user_firstname'])) {
                            printf("%s", $_POST['user_firstname']);
                        } ?>" class="input" type="text" name="user_firstname" placeholder="Prenom"> <br>
        <input value="<?php if (isset($_POST['user_email'])) {
                            printf("%s", $_POST['user_email']);
                        } ?>" class="input" type="email" name="user_email" placeholder="Email"> <br>
        <input class="input" type="password" name="password" placeholder="Password" autocomplete="new-password"> <br>
        <input class="input" type="password" name="verifpassword" placeholder="Verification password"> <br>
        <hr>

        <input class="input form_btn" type="submit" name="submit" value="S'inscrire">

    </form>

    <p>Deja un compte ? <a href="connexion.php"><em>cliquer ici</em></a></p>

</div>


<!-- --------------------------------------------------------------------------------------- -->
<!-- END BODY -->
<!-- --------------------------------------------------------------------------------------- -->
<?php
include 'include/footer.php';
?>